[[_TOC_]]

# Background
Vagrant and VirtualBox (or some other VM provider) can be used to quickly build or rebuild virtual servers.
This Vagrant profile installs Kubernetes using the Ansible provisioner.


# Setup

## Requirements
This project relies on several external packages
* `Vagrant` since `2.2.x`
* `Ansible` since `2.9.x`
* optional: `vagrant-proxyconf` since `1.5.x`
* optional: `helm` since `3.0.x`

## Start
Just type `vagrant up` and enjoy...

## Vagrant

```bash
> vagrant up # be patient, good things take time
> vagrant ssh-config >> ~/.ssh/config
> vagrant ssh master
[vagrant@master ~]$ kubectl get nodes
```

## Behind a corporate proxy server

```bash
> vagrant plugin install vagrant-proxyconf
> export HTTP_PROXY=http://proxy.example.com:8080; export HTTPS_PROXY=$HTTP_PROXY; export NO_PROXY=localhost,127.0.0.1
> vagrant up
```

* see [plugin home page](http://tmatilai.github.io/vagrant-proxyconf/) for more information about proxy setup


# Kubernetes

* `kubernetes-admin` kubeconfig file is automatically generated during the installation
  * the config file allows full administrative rights to the cluster
  * the config file is also copied to:
    * `master:/root/.kube/config`
    * `master:/home/vagrant/.kube/config`

## Cluster

* Number of nodes in cluster and their parameters (CPU, memory) can be configured in `nodes` definition in [Vagrantfile](Vagrantfile#L17).

## User Management

* script to create a new user (and namespace) is located in `/home/vagrant/bin/kuusma.sh` on `master`

```bash
> vagrant ssh master
[vagrant@master ~]$ kuusma.sh # to get help
[vagrant@master ~]$ kuusma.sh dragon
[vagrant@master ~]$ cat ~/.kube/dragon-k8s-config
```

## Access from Vagrant host

```bash
> scp master:/home/vagrant/.kube/<username>-k8s-config /path/to/.kube/config
> sed -i 's/10.0.0.10/127.0.0.1/g' /path/to/.kube/config
> kubectl version --kubeconfig=/path/to/.kube/config
```

# Kubernetes Dashboard

* Installed with granted Admin privileges (might be a security risk)
  * ref: https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/README.md#admin-privileges

## Access from Vagrant host

```bash
# start proxy on master
[vagrant@master ~]$ kubectl proxy --address='0.0.0.0'
# or from Vagrant host
> ssh -t master kubectl proxy --address='0.0.0.0'
```

* will make Dashboard available at [here](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/)

## Log in

* AuthN with `Kubeconfig` doesn't work when using certificate authentication (which is what we do in this cluster)
  * ref: https://github.com/kubernetes-incubator/kube-aws/issues/1051
* Bearer Token works -> to get the token:

```bash
[vagrant@master ~]$ kubectl -n kubernetes-dashboard describe secrets $(kubectl -n kubernetes-dashboard get secret | grep -oP '^kubernetes-dashboard-token-[a-z0-9]*') | grep '^token:'
```

* copy the token and paste it into Enter token field on login screen


# Argo Workflows
* instaled in namespace `argo`
* Argo CLI installed in `/home/vagrant/bin/` on `master`

## Argo Workflows UI

```bash
> kubectl port-forward service/argo-ui -n argo 8080:80
```

* will make Argo UI available at [here](http://localhost:8080)

## Argo Events
* instaled in namespace `argo-events`
* simple workflow with webhook trigger prepared in `Helm3/calculator/`


```bash
> helm install calc ./Helm3/calculator
> helm test calc
```

* or start the workflow with webhook

```bash
> kubectl port-forward service/webhook4calculator-gateway-svc -n argo-events 12000:12000
> curl -d '{"a":"7","b":"5","operation":"multiply"}' -H "Content-Type: application/json" -X POST http://localhost:12000/calculator
> argo list
```


# Limitations

* multi-master for Kubernetes HA is not supported at the moment
  * do not include more masters into `nodes` array in `Vagrantfile`
* works with CentOS/7 only, support for CentOS/8 in progress


# Helm3 Demo

* demo Helm chart located in `./Helm3/sinatra` (simple Ruby/Sintra hello-world application)
* see exposed endpoints in [.Values.image.command](Helm3/sinatra/values.yaml#L11):
  * `/hi/:name` - gets greeting to :name
  * `/healthy` - just a healt check

```bash
> helm install sinatra ./Helm3/sinatra
> helm test sinatra
# and to access the endpoints
> kubectl port-forward service/sinatra -n default 8080:80
> curl localhost:8080/hi/Bowie
```
