#!/bin/sh

# Delete all resources of Kubernetes Dashboard.

kubectl delete clusterrolebinding kubernetes-dashboard
kubectl delete clusterrole kubernetes-dashboard
kubectl delete namespace kubernetes-dashboard
