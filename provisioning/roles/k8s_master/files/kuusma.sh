#!/bin/sh

# Kubernetes User Management.
#
# This script creates a new user in K8s cluster and provides his kubeconfig file.
#
# kubeconfig file consists of
# * cluster configuration (Name, URL, CA cert)
# * user configuration (name, key, cert)
# * context configuration

display_usage() {
  echo -e "Usage: kum.sh USERNAME [NAMESPACE]"
  echo -e "Arguments:"
  echo -e "\tUSERNAME\tusername in K8s cluster, ~/.kube/\$USERNAME-k8s-config will be generated"
  echo -e "\tNAMESPACE\tnamespace for the user, 'default' if not provided"
}
if [ $# -eq 0 ] ; then
    display_usage
    exit 1
fi


WDIR=$HOME/.kube/kum
USERNAME=$1
NAMESPACE=${2:-default}

mkdir -p $WDIR

# 1.
# create a key and certificate signing request (CSR) for the user
openssl req -new -newkey rsa:4096 -nodes -keyout $WDIR/$USERNAME-k8s.key -out $WDIR/$USERNAME-k8s.csr -subj "/CN=${USERNAME}/O=devops"
REQ=$(cat $WDIR/$USERNAME-k8s.csr | base64 | tr -d '\n')

# delete previous stuff if exists
kubectl delete csr $USERNAME-k8s-access
kubectl delete clusterrolebinding $USERNAME-admin
kubectl delete rolebinding $USERNAME-admin

# sign the request by the cluster CA
cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: $USERNAME-k8s-access
spec:
  groups:
  - system:authenticated
  request: $REQ
  usages:
  - client auth
EOF

# approve the CSR object
kubectl certificate approve $USERNAME-k8s-access

# retrieve the user's certificate
kubectl get csr $USERNAME-k8s-access -o jsonpath='{.status.certificate}' | base64 --decode > $WDIR/$USERNAME-k8s-access.crt

# get the cluster CA certificate
kubectl config view -o jsonpath='{.clusters[0].cluster.certificate-authority-data}' --raw | base64 --decode - > $WDIR/k8s-ca.crt


# 2.
# set up the cluster configuration in user’s kubeconfig file
kubectl config set-cluster $(kubectl config view -o jsonpath='{.clusters[0].name}') --server=$(kubectl config view -o jsonpath='{.clusters[0].cluster.server}') --certificate-authority=$WDIR/k8s-ca.crt --kubeconfig=$HOME/.kube/$USERNAME-k8s-config --embed-certs

# import user’s key and cert into the file
kubectl config set-credentials $USERNAME --client-certificate=$WDIR/$USERNAME-k8s-access.crt --client-key=$WDIR/$USERNAME-k8s.key --embed-certs --kubeconfig=$HOME/.kube/$USERNAME-k8s-config


# 3.
#  create a context
kubectl config set-context $USERNAME --cluster=$(kubectl config view -o jsonpath='{.clusters[0].name}') --namespace=$NAMESPACE --user=$USERNAME --kubeconfig=$HOME/.kube/$USERNAME-k8s-config

# specify the context that user will use for his kubectl commands
kubectl config use-context $USERNAME --kubeconfig=$HOME/.kube/$USERNAME-k8s-config

# create specific namespace if provided
if [ "$NAMESPACE" != "default" ]; then
    kubectl create ns $NAMESPACE
fi

# let's test
kubectl version --kubeconfig=$HOME/.kube/$USERNAME-k8s-config


# 4.
# give user a role that will give him complete freedom (within the given namespace if provided)
if [ "$NAMESPACE" = "default" ]; then
    kubectl create clusterrolebinding $USERNAME-admin --clusterrole=cluster-admin --user=$USERNAME
else
    kubectl create rolebinding $USERNAME-admin --namespace=$NAMESPACE --clusterrole=admin --user=$USERNAME
fi
