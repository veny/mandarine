# Author: vaclav.sykora@protonmail.ch
# Description: Installs Docker Engine and setup Kubernetes cluster
#
# Optional plugins:
#     vagrant-proxyconf (if you don't have direct access to the Internet)
#         see https://github.com/tmatilai/vagrant-proxyconf for configuration
#
# Usage:
# > vagrant plugin install vagrant-proxyconf  # optional, in case you are behind a corporate proxy server
# > vagrant up
# > vagrant ssh-config >> ~/.ssh/config
# > sudo vi /etc/hosts
#   * add '127.0.0.1 master node1 node2'


# use two digits id(s)
nodes = [
  { :id => '10', :hostname => 'master',  :ip => '10.0.0.10', :memory => 2048, :cpus => 2 },
  { :id => '11', :hostname => 'node1',   :ip => '10.0.0.11', :memory => 4096, :cpus => 2 },
#  { :id => '12', :hostname => 'node2',   :ip => '10.0.0.12', :memory => 2048, :cpus => 2 },
]

domain   = 'mandarine.k8s'
hosts = nodes.map { |h| "#{h[:ip]} #{h[:hostname]} #{h[:hostname]}.#{domain}" }.join("\n")

Vagrant.configure('2') do |config|
  config.ssh.insert_key = false

  if Vagrant.has_plugin?('vagrant-proxyconf')
    ['http_proxy', 'HTTP_PROXY', 'https_proxy', 'HTTPS_PROXY', 'no_proxy', 'NO_PROXY'].each do |var|
      if proxy = ENV[var]
        var.downcase!
        attr = (var[/^no_/] ? var : var.sub!(/_proxy/, '')).to_sym
        if :no_proxy == attr
          cluster_no_proxy = [proxy, ".#{domain}"]
          cluster_no_proxy.push(nodes.map { |h| h[:hostname] }).push(nodes.map { |h| h[:ip] })
          proxy = "#{cluster_no_proxy.flatten.join(',')}"
        end
        config.proxy.send("#{attr}=", proxy)
        puts ">> #{var}: #{config.proxy.send(attr)}"
      end
    end
  end

  nodes.each do |node|
    config.vm.define node[:hostname] do |nodeconfig|
      nodeconfig.vm.box = 'centos/7'
      nodeconfig.vm.hostname = node[:hostname]
      nodeconfig.vm.network :private_network, ip: node[:ip], virtualbox__intnet: domain
      nodeconfig.vm.network :forwarded_port, guest: 6443, host: 6443 if node == nodes[0]
      nodeconfig.vm.network :forwarded_port, guest: 8001, host: 8001 if node == nodes[0] # Dashboard
      nodeconfig.vm.provider :virtualbox do |vb|
        vb.name = "#{node[:hostname]}.#{domain}"
        vb.memory = node[:memory]
        vb.cpus = node[:cpus]
        vb.linked_clone = true
        vb.customize ['modifyvm', :id, '--natdnshostresolver1', 'on']
        vb.customize ['modifyvm', :id, '--natdnsproxy1', 'on']
        vb.customize ['modifyvm', :id, '--macaddress1', "5CA1AB1E00#{node[:id]}"]
        vb.customize ['modifyvm', :id, '--natnet1', '192.168/16']
      end
      nodeconfig.vm.provision 'ansible' do |ansible|
        ansible.playbook = 'provisioning/playbook.yml'
        ansible.extra_vars = { hostnames: hosts }
        ansible.groups = {
          'masters' => nodes.select { |h| h[:hostname] =~ /master/ }.map { |a| a[:hostname] },
          'nodes' => nodes.select { |h| h[:hostname] =~ /node/ }.map { |a| a[:hostname] }
        }
      end
    end
  end
end
